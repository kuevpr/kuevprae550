#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>   // SIGINT and SIGTERM defines for signal interrupts
#include <errno.h>    // Error type definitions such as EINVAL or EBUSY
#include <pthread.h>
#include <float.h>
#include <sys/time.h>
#include <unistd.h>


#include "lcm/lcm.h"

#include "io/comms.h"
#include "io/serial.h"

#include "util/pwmimu.h"

#include "util/time_util.h"

#include "../include/lcmtypes/lcmtypes_c/imu_data_t.h"
#include "../include/lcmtypes/lcmtypes_c/kill_t.h"
#include "../include/lcmtypes/lcmtypes_c/cfg_uart_baud_t.h"
#include "../include/lcmtypes/lcmtypes_c/channels_t.h"
#include "../include/lcmtypes/lcmtypes_c/kill_t.h"
#include "../include/lcmtypes/lcmtypes_c/led_t.h"
#include "../include/lcmtypes/lcmtypes_c/cfg_data_frequency_t.h"
#include "../include/lcmtypes/lcmtypes_c/cfg_usb_serial_num_t.h"

const char *USAGE = "Usage: ./comms_driver <-x xbee_dev_path> <-u usb_dev_path> "
                    "[-i <pwmimu_id> -r <roll_channel> -p <pitch_channel> "
                    "-c <imu_output_channel>]";
bool loopback_mode = false;
static bool verbose = false;
static bool very_verbose = false;

static uint16_t pwmimu_id = 2;
static uint8_t  r_channel = 6;
static uint8_t  p_channel = 7;
static char *imu_channel = "IMU_DJI";


#define verbose_printf(...) \
    do{\
        if(verbose) printf(__VA_ARGS__);\
    }while(0)\

#define very_verbose_printf(...) \
    do{\
        if(very_verbose) printf(__VA_ARGS__);\
    }while(0)\

static void* xbee_run(void*);
static void* usb_run(void*);
static void publish_xbee(container_t *data, uint16_t id);
static void publish_usb (container_t *data, uint16_t id);
static void handler_channels(void *usr, uint16_t id, comms_channel_t channel,
                             const uint8_t *msg, uint16_t len);
static void handler_channels_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                 const channels_t *msg, void *user);
static void handler_channels_output(void *usr, uint16_t id, comms_channel_t channel,
                                    const uint8_t *msg, uint16_t len);
static void handler_channels_output_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                        const channels_t *msg, void *user);
static void handler_channels_default(void *usr, uint16_t id, comms_channel_t channel,
                                     const uint8_t *msg, uint16_t len);
static void handler_channels_default_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                         const channels_t *msg, void *user);
static void handler_kill(void *usr, uint16_t id, comms_channel_t channel,
                         const uint8_t *msg, uint16_t len);
static void handler_kill_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                             const kill_t *msg, void *user);
static void handler_led(void *usr, uint16_t id, comms_channel_t channel,
                         const uint8_t *msg, uint16_t len);
static void handler_led_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                             const led_t *msg, void *user);
static void handler_cfg_usb_serial_num(void *usr, uint16_t id, comms_channel_t channel,
                                       const uint8_t *msg, uint16_t len);
static void handler_cfg_usb_serial_num_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                           const cfg_usb_serial_num_t *msg, void *user);
static void handler_cfg_data_frequency(void *usr, uint16_t id, comms_channel_t channel,
                                       const uint8_t *msg, uint16_t len);
static void handler_cfg_data_frequency_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                           const cfg_data_frequency_t *msg, void *user);
static void handler_cfg_uart_baud(void *usr, uint16_t id, comms_channel_t channel,
                                  const uint8_t *msg, uint16_t len);
static void handler_cfg_uart_baud_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                      const cfg_uart_baud_t *msg, void *user);

pthread_t xbee_thread;
pthread_mutex_t xbee_lock;
comms_t *xbee_comms;
serial_t *xbee = NULL;

pthread_t usb_thread;
pthread_mutex_t usb_lock;
comms_t *usb_comms;
serial_t *usb = NULL;

lcm_t *lcm;

volatile bool done = false;
void interrupt(int sig)
{
    static int8_t num_exit_attempts = 0;
    num_exit_attempts++;
    if(num_exit_attempts == 1)
        fprintf(stderr, "Caught ctrl+c and signalling exit to driver\n");
    if(num_exit_attempts == 3)
    {
        fprintf(stderr, "Force quitting\n");
        exit(1);
    }

    done = true;
}


int main(int argc, char *argv[])
{
    uint8_t i;
    for(i = 0; i < argc; ++i)
    {
        if((strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0))
        {
            fprintf(stdout, "%s\n", USAGE);
            exit(0);
        }
        if(strcmp(argv[i], "-v") == 0)
        {
            verbose = true;
            fprintf(stdout, "Running in verbose mode\n");
        }
        if(strcmp(argv[i], "-vv") == 0)
        {
            very_verbose = true;
            verbose = true;
            fprintf(stdout, "Running in very verbose mode\n");
        }
        if(strcmp(argv[i], "-l") == 0)
        {
            loopback_mode = true;
            fprintf(stdout, "Running in loopback mode\n");
        }
        if(strcmp(argv[i], "-i") == 0)
        {
            if(i + 1 < argc)
                pwmimu_id = atoi(argv[++i]);
            break;
        }
        if(strcmp(argv[i], "-r") == 0)
        {
            if(i + 1 < argc)
                r_channel = atoi(argv[++i]);
            break;
        }
        if(strcmp(argv[i], "-p") == 0)
        {
            if(i + 1 < argc)
                p_channel = atoi(argv[++i]);
            break;
        }
        if(strcmp(argv[i], "-c") == 0)
        {
            if(i + 1 < argc)
                imu_channel = argv[++i];
            break;
        }
    }

    (void) signal(SIGINT, interrupt);
    (void) signal(SIGTERM, interrupt);

    lcm = lcm_create(NULL);
    while(lcm == NULL)
    {
        fprintf(stderr, "LCM failed to initialize. Reattempting....\n");
        usleep(1000000);
        lcm = lcm_create(NULL);
    }


    // Create Serial Device for XBee
    char *xbee_dev_name = "/dev/XBee";
    for(i = 0; i < argc; ++i)
    {
        if(strcmp(argv[i], "-x") == 0)
        {
            if(i + 1 < argc)
                xbee_dev_name = argv[++i];
            break;
        }
    }
    //xbee = serial_create(xbee_dev_name, B9600);
    xbee = serial_create(xbee_dev_name, B115200);
    if(xbee == NULL)
        fprintf(stderr, "XBee device does not exist at %s\n", xbee_dev_name);
    else
        fprintf(stdout, "XBee device successfully opened at 9600 baud on %s\n", xbee_dev_name);

    // Create Serial Device for USB
    char *usb_dev_name = "/dev/stack";
    for(i = 0; i < argc; ++i)
    {
        if(strcmp(argv[i], "-u") == 0)
        {
            if(i + 1 < argc)
                usb_dev_name = argv[++i];
            break;
        }
    }
    usb = serial_create(usb_dev_name, B115200);
    if(usb == NULL)
        fprintf(stderr, "Usb device does not exist at %s\n", usb_dev_name);
    else
        fprintf(stdout, "Usb device successfully opened at 115200 baud on %s\n", usb_dev_name);


    // If no open comm ports and not in loopback mode, quit.
    if(!xbee && !usb && !loopback_mode)
    {
        fprintf(stderr, "Failed to open any comms devices. Exiting...\n");
        lcm_destroy(lcm);
        exit(1);
    }


    // Create comms devices
    if(xbee)
    {
        xbee_comms = comms_create(1000, 1000, 1, publish_xbee);
        comms_subscribe(xbee_comms, CHANNEL_KILL, handler_kill, NULL);
        //comms_subscribe(xbee_comms, CHANNEL_LED, handler_led, NULL);
        comms_subscribe(xbee_comms, CHANNEL_CFG_CHANNELS_DEFAULT, handler_channels_default, NULL);
        comms_subscribe(xbee_comms, CHANNEL_CHANNELS, handler_channels, NULL);
        comms_subscribe(xbee_comms, CHANNEL_CHANNELS_OUTPUT, handler_channels_output, NULL);
        comms_subscribe(xbee_comms, CHANNEL_CFG_USB_SN, handler_cfg_usb_serial_num, NULL);
        comms_subscribe(xbee_comms, CHANNEL_CFG_DATA_FREQUENCY, handler_cfg_data_frequency, NULL);
        comms_subscribe(xbee_comms, CHANNEL_CFG_UART_BAUD, handler_cfg_uart_baud, NULL);
        pthread_mutex_init(&xbee_lock, NULL);
        pthread_create(&xbee_thread, NULL, xbee_run, NULL);
    }

    // Create comms devices
    if(usb)
    {
        usb_comms = comms_create(1000, 1000, 1, publish_usb);
        comms_subscribe(usb_comms, CHANNEL_KILL, handler_kill, NULL);
        //comms_subscribe(usb_comms, CHANNEL_LED, handler_led, NULL);
        comms_subscribe(usb_comms, CHANNEL_CFG_CHANNELS_DEFAULT, handler_channels_default, NULL);
        comms_subscribe(usb_comms, CHANNEL_CHANNELS, handler_channels, NULL);
        comms_subscribe(usb_comms, CHANNEL_CHANNELS_OUTPUT, handler_channels_output, NULL);
        comms_subscribe(usb_comms, CHANNEL_CFG_USB_SN, handler_cfg_usb_serial_num, NULL);
        comms_subscribe(usb_comms, CHANNEL_CFG_DATA_FREQUENCY, handler_cfg_data_frequency, NULL);
        comms_subscribe(usb_comms, CHANNEL_CFG_UART_BAUD, handler_cfg_uart_baud, NULL);
        pthread_mutex_init(&usb_lock, NULL);
        pthread_create(&usb_thread, NULL, usb_run, NULL);
    }

    kill_t_subscription_t  *kill_subs =
        kill_t_subscribe(lcm, "KILL_.*_TX", handler_kill_lcm, NULL);
    //led_t_subscription_t  *led_subs =
    //    led_t_subscribe(lcm, "LED_.*_TX", handler_led_lcm, NULL);
    channels_t_subscription_t *channels_subs =
        channels_t_subscribe(lcm, "CHANNELS_.*_TX", handler_channels_lcm, NULL);
    channels_t_subscription_t *channels_output_subs =
        channels_t_subscribe(lcm, "OUTPUTS_.*_TX", handler_channels_output_lcm, NULL);
    channels_t_subscription_t *channels_default_subs =
        channels_t_subscribe(lcm, "CFG_CHANNELS_DEFAULT_.*_TX", handler_channels_default_lcm, NULL);
    cfg_data_frequency_t_subscription_t *data_freq_subs =
        cfg_data_frequency_t_subscribe(lcm, "CFG_DATA_FREQUENCY_.*_TX",
                                       handler_cfg_data_frequency_lcm, NULL);
    cfg_usb_serial_num_t_subscription_t *usb_serial_num_subs =
        cfg_usb_serial_num_t_subscribe(lcm, "CFG_USB_SERIAL_NUM_.*_TX",
                                       handler_cfg_usb_serial_num_lcm, NULL);
    cfg_uart_baud_t_subscription_t *uart_baud_subs =
        cfg_uart_baud_t_subscribe(lcm, "CFG_UART_BAUD_.*_TX",
                                       handler_cfg_uart_baud_lcm, NULL);

    while(!done)
    {
        // Set up timeout on lcm_handle to prevent blocking at program exit
        int lcm_fd = lcm_get_fileno(lcm);
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(lcm_fd, &fds);

        struct timeval timeout = { 0, 100000 }; // wait 100ms
        int status = select(lcm_fd + 1, &fds, NULL, NULL, &timeout);

        if(done) break;

        if(status != 0 && FD_ISSET(lcm_fd, &fds)) {
            if ( lcm_handle(lcm) ) {
                fprintf( stderr, "LCM handle error, aborting\n" );
                done = true;
            }
        }

        if(usb)
        {
            pthread_mutex_lock(&usb_lock);
                comms_status_t status = comms_transmit(usb_comms);
            pthread_mutex_unlock(&usb_lock);
            if(status != COMMS_STATUS_DONE)
            {
                char buf[50];
                fprintf(stderr, "usb transmit status = %s\n", comms_status_to_str(status, buf, sizeof(buf)));
            }
        }

        if(xbee)
        {
            pthread_mutex_lock(&xbee_lock);
                comms_status_t status = comms_transmit(xbee_comms);
            pthread_mutex_unlock(&xbee_lock);
            if(status != COMMS_STATUS_DONE)
            {
                char buf[50];
                fprintf(stderr, "xbee transmit status = %s\n", comms_status_to_str(status, buf, sizeof(buf)));
            }
        }
    }
    fprintf(stdout, "Lcm driver destroyed.\n");

    kill_t_unsubscribe(lcm, kill_subs);
    //led_t_unsubscribe(lcm, led_subs);
    channels_t_unsubscribe(lcm, channels_subs);
    channels_t_unsubscribe(lcm, channels_output_subs);
    channels_t_unsubscribe(lcm, channels_default_subs);
    cfg_data_frequency_t_unsubscribe(lcm, data_freq_subs);
    cfg_usb_serial_num_t_unsubscribe(lcm, usb_serial_num_subs);
    cfg_uart_baud_t_unsubscribe(lcm, uart_baud_subs);

    if(xbee)
    {
        pthread_cancel(xbee_thread);
        pthread_mutex_destroy(&xbee_lock);
        comms_destroy(xbee_comms);
        serial_destroy(xbee);
        fprintf(stdout, "Xbee driver destroyed.\n");
    }

    if(usb)
    {
        pthread_cancel(usb_thread);
        pthread_mutex_destroy(&usb_lock);
        comms_destroy(usb_comms);
        serial_destroy(usb);
        fprintf(stdout, "Usb driver destroyed.\n");
    }

    lcm_destroy(lcm);

    return 0;
}



uint8_t parse_id(const char *channel_name)
{
    static char *suffix = "_TX";

    uint8_t suffix_len = strlen(suffix);
    uint8_t len = strlen(channel_name);

    if(suffix_len > len) return 0;

    // If string ends with _TX
    if(strncmp(channel_name + len - suffix_len, suffix, suffix_len) == 0)
    {
        // search backwards for id where channel name could be in form of "..._id_TX"
        uint8_t i;
        uint8_t start_idx = len - 1 - suffix_len;
        for(i = start_idx; i >= 0; --i)
        {
            if(channel_name[i] == '_')
            {
                char *id_str = malloc(sizeof(char) * (start_idx - i + 1));
                id_str[start_idx - i] = '\0';
                strncpy(id_str, &channel_name[i+1], start_idx - i);
                uint8_t id = atoi(id_str);
                free(id_str);
                return id;
                break;
            }
        }
    }

    return 0;
}


static void* xbee_run(void* arg)
{
    fprintf(stdout, "Starting xbee receive thread\n");

    char data[1000];
    while(!done)
    {
        uint16_t num_read;
        if(num_read = serial_read(xbee, data, 1000))
        {
            uint16_t i;
            for(i = 0; i < num_read; ++i)
            {
                very_verbose_printf("Received xbee byte: %x \n", data[i]);
                if(!comms_handle_now(xbee_comms, data[i]))
                {
                    fprintf(stderr, "Failed to buffer byte received on xbee. Buffer full\n");
                }
            }
        }

        if (verbose) {
            static uint64_t last_time = 0;
            if(timestamp_now() - last_time > 2e5)
            {
                last_time = timestamp_now();
                COMMS_METADATA_PRINTF(xbee_comms);
                printf("\n");
            }
        }
    }
    return NULL;
}

static void publish_xbee(container_t *data, uint16_t id)
{
    verbose_printf("TX UART: ");
    uint16_t i, data_len = comms_cfuncs->size(data);
    for(i = 0; i < data_len; ++i)
        verbose_printf("%x ", *(const uint8_t*)comms_cfuncs->at(data,i));
    verbose_printf("\n");

    while(comms_cfuncs->size(data) > 0)
    {
        serial_write(xbee, (const uint8_t*)comms_cfuncs->front(data), 1);
        comms_cfuncs->remove_front(data);
    }
}

static void* usb_run(void* arg)
{
    fprintf(stdout, "Starting usb receive thread\n");
    char data[1000];
    while(!done)
    {
        uint16_t num_read;
        if(num_read = serial_read(usb, data, 1000))
        {
            uint16_t i;
            for(i = 0; i < num_read; ++i)
            {
                very_verbose_printf("Received usb byte: %x \n", data[i]);
                if(!comms_handle_now(usb_comms, data[i]))
                {
                    fprintf(stderr, "Failed to buffer byte received on usb. Buffer full\n");
                }
            }
        }

        if (verbose) {
            static uint64_t last_time = 0;
            if(timestamp_now() - last_time > 2e5)
            {
                last_time = timestamp_now();
                COMMS_METADATA_PRINTF(usb_comms);
                printf("\n");
            }
        }
    }
    return NULL;
}

static void publish_usb (container_t *data, uint16_t id)
{
    verbose_printf("TX USB: ");
    uint16_t i, data_len = comms_cfuncs->size(data);
    for(i = 0; i < data_len; ++i)
        verbose_printf("%x ", *(const uint8_t*)comms_cfuncs->at(data,i));
    verbose_printf("\n");

    while(comms_cfuncs->size(data) > 0)
    {
        serial_write(usb, (const uint8_t*)comms_cfuncs->front(data), 1);
        comms_cfuncs->remove_front(data);
    }
}

static void handler_kill(void *usr, uint16_t id, comms_channel_t channel,
                         const uint8_t *msg, uint16_t len)
{
    char lcm_channel[20];
    snprintf(lcm_channel, 20, "KILL_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    kill_t kill;
    memset(&kill, 0, sizeof(kill_t));
    __kill_t_decode_array(msg, 0, len, &kill, 1);
    kill_t_publish(lcm, lcm_channel, &kill);
    kill_t_decode_cleanup(&kill);
}

static void handler_kill_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                             const kill_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __kill_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __kill_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_KILL, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_KILL, buf, len, 0);
    if(loopback_mode) handler_kill(NULL, id, CHANNEL_KILL, buf, len);
    free(buf);
}

/*
static void handler_led(void *usr, uint16_t id, comms_channel_t channel,
                         const uint8_t *msg, uint16_t len)
{
    char lcm_channel[20];
    snprintf(lcm_channel, 20, "LED_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    led_t led;
    memset(&led, 0, sizeof(led_t));
    __led_t_decode_array(msg, 0, len, &led, 1);
    led_t_publish(lcm, lcm_channel, &led);
    led_t_decode_cleanup(&led);
}

static void handler_led_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                             const led_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __led_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __led_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_LED, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_LED, buf, len, 0);
    if(loopback_mode) handler_led(NULL, id, CHANNEL_LED, buf, len);
    free(buf);
}
*/
static void handler_channels(void *usr, uint16_t id, comms_channel_t channel,
                             const uint8_t *msg, uint16_t len)
{
    char lcm_channel[40];
    snprintf(lcm_channel, 40, "CHANNELS_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    channels_t channels;
    memset(&channels, 0, sizeof(channels_t));
    __channels_t_decode_array(msg, 0, len, &channels, 1);
    channels_t_publish(lcm, lcm_channel, &channels);

    // Extract the roll and pitch from the channels
    if (id == pwmimu_id) {
        double r = a2_pwm_to_rp(channels.channels[r_channel]);
        double p = a2_pwm_to_rp(channels.channels[p_channel]);

        if(r != DBL_MAX && p != DBL_MAX)
        {
            imu_data_t imu;
            imu.utime = channels.utime;
            imu.attitude[0] = r;
            imu.attitude[1] = p;

            imu.flags = 0;
            imu.flags = imu.flags | IMU_DATA_T_FLAGS_TYPE_GIMBALOUTPUT;
            imu.flags = imu.flags | IMU_DATA_T_FLAGS_ATTITUDE_RPH;

            imu_data_t_publish(lcm, imu_channel, &imu);
        }
    }

    __channels_t_decode_array_cleanup(&channels, 1);
}

static void handler_channels_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                 const channels_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __channels_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __channels_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_CHANNELS, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_CHANNELS, buf, len, 0);
    if(loopback_mode) handler_channels(NULL, id, CHANNEL_CHANNELS, buf, len);
    free(buf);
}

static void handler_channels_output(void *usr, uint16_t id, comms_channel_t channel,
                                    const uint8_t *msg, uint16_t len)
{
    char lcm_channel[40];
    snprintf(lcm_channel, 40, "OUTPUTS_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    channels_t channels;
    memset(&channels, 0, sizeof(channels_t));
    __channels_t_decode_array(msg, 0, len, &channels, 1);
    channels_t_publish(lcm, lcm_channel, &channels);

    __channels_t_decode_array_cleanup(&channels, 1);
}

static void handler_channels_output_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                        const channels_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __channels_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __channels_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_CHANNELS_OUTPUT, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_CHANNELS_OUTPUT, buf, len, 0);
    if(loopback_mode) handler_channels(NULL, id, CHANNEL_CHANNELS_OUTPUT, buf, len);
    free(buf);
}

static void handler_channels_default(void *usr, uint16_t id, comms_channel_t channel,
                                     const uint8_t *msg, uint16_t len)
{
    char lcm_channel[40];
    snprintf(lcm_channel, 40, "CFG_CHANNELS_DEFAULT_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    channels_t channels;
    memset(&channels, 0, sizeof(channels_t));
    __channels_t_decode_array(msg, 0, len, &channels, 1);
    channels_t_publish(lcm, lcm_channel, &channels);
    __channels_t_decode_array_cleanup(&channels, 1);
}

static void handler_channels_default_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                         const channels_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __channels_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __channels_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_CFG_CHANNELS_DEFAULT, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_CFG_CHANNELS_DEFAULT, buf, len, 0);
    if(loopback_mode) handler_channels_default(NULL, id, CHANNEL_CFG_CHANNELS_DEFAULT, buf, len);
    free(buf);
}

static void handler_cfg_usb_serial_num(void *usr, uint16_t id, comms_channel_t channel,
                                       const uint8_t *msg, uint16_t len)
{
    char lcm_channel[40];
    snprintf(lcm_channel, 40, "CFG_USB_SERIAL_NUM_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    cfg_usb_serial_num_t cfg_usb_serial_num;
    memset(&cfg_usb_serial_num, 0, sizeof(cfg_usb_serial_num_t));
    __cfg_usb_serial_num_t_decode_array(msg, 0, len, &cfg_usb_serial_num, 1);
    cfg_usb_serial_num_t_publish(lcm, lcm_channel, &cfg_usb_serial_num);
    __cfg_usb_serial_num_t_decode_array_cleanup(&cfg_usb_serial_num, 1);
}

static void handler_cfg_usb_serial_num_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                           const cfg_usb_serial_num_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __cfg_usb_serial_num_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __cfg_usb_serial_num_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_CFG_USB_SN, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_CFG_USB_SN, buf, len, 0);
    if(loopback_mode) handler_cfg_usb_serial_num(NULL, id, CHANNEL_CFG_USB_SN, buf, len);
    free(buf);
}

static void handler_cfg_data_frequency(void *usr, uint16_t id, comms_channel_t channel,
                                       const uint8_t *msg, uint16_t len)
{
    char lcm_channel[40];
    snprintf(lcm_channel, 40, "CFG_DATA_FREQUENCY_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    cfg_data_frequency_t cfg_data_frequency;
    memset(&cfg_data_frequency, 0, sizeof(cfg_data_frequency_t));
    __cfg_data_frequency_t_decode_array(msg, 0, len, &cfg_data_frequency, 1);
    cfg_data_frequency_t_publish(lcm, lcm_channel, &cfg_data_frequency);
    __cfg_data_frequency_t_decode_array_cleanup(&cfg_data_frequency, 1);
}

static void handler_cfg_data_frequency_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                           const cfg_data_frequency_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __cfg_data_frequency_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __cfg_data_frequency_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_CFG_DATA_FREQUENCY, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_CFG_DATA_FREQUENCY, buf, len, 0);
    if(loopback_mode) handler_cfg_data_frequency(NULL, id, CHANNEL_CFG_DATA_FREQUENCY, buf, len);
    free(buf);
}

static void handler_cfg_uart_baud(void *usr, uint16_t id, comms_channel_t channel,
                                  const uint8_t *msg, uint16_t len)
{
    char lcm_channel[40];
    snprintf(lcm_channel, 40, "CFG_UART_BAUD_%d_RX", id);

    verbose_printf("%s: ", lcm_channel);
    uint16_t i;
    for(i = 0; i < len; ++i)
    {
        verbose_printf("%x ", msg[i]);
    }
    verbose_printf("\n");

    cfg_uart_baud_t cfg_uart_baud;
    memset(&cfg_uart_baud, 0, sizeof(cfg_uart_baud_t));
    __cfg_uart_baud_t_decode_array(msg, 0, len, &cfg_uart_baud, 1);
    cfg_uart_baud_t_publish(lcm, lcm_channel, &cfg_uart_baud);
    __cfg_uart_baud_t_decode_array_cleanup(&cfg_uart_baud, 1);
}

static void handler_cfg_uart_baud_lcm(const lcm_recv_buf_t *rbuf, const char *channel,
                                      const cfg_uart_baud_t *msg, void *user)
{
    uint8_t id = parse_id(channel);

    verbose_printf("Received msg on lcm channel %s with id %d\n", channel, id);

    uint32_t maxlen = __cfg_uart_baud_t_encoded_array_size(msg, 1);
    uint8_t *buf = (uint8_t *) malloc(sizeof(uint8_t) * maxlen);
    uint32_t len = __cfg_uart_baud_t_encode_array(buf, 0, maxlen, msg, 1);
    if(usb)  comms_publish( usb_comms, id, CHANNEL_CFG_UART_BAUD, buf, len, 0);
    if(xbee) comms_publish(xbee_comms, id, CHANNEL_CFG_UART_BAUD, buf, len, 0);
    if(loopback_mode) handler_cfg_uart_baud(NULL, id, CHANNEL_CFG_UART_BAUD, buf, len);
    free(buf);
}
