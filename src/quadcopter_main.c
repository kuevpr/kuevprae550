//
// Top-level program for quadcopter
//
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#define EXTERN   // main() program definition

#include "../include/quadcopter_main.h"

/*  ########################################
    ###    QUADCOPTER MAIN FUNCTION      ### 
    ########################################

    Code has several threads. 
    Comment the ones you do not need.
    List below show the threads you NEED to run it.

    ### FOR THE QUADROTOR TO FLY ###
    -> lcm_thread_loop
    -> processing_loop
    -> run_motion_capture
    -> (if you use servos) run_dynamixel_comm
    -> (if you use imu)    run_imu

    ### FOR TESTING MOTION CAPTURE ONLY ###
    -> run_motion_capture

    ### FOR TESTING SERVOS ONLY (NOT CONNECTED TO YOUR GRIPPER!) ###
    -> run_dynamixel_comm
    -> set_dynamixel
*/

int main() {
  wpCount = 0;        //Number of waypoints read in
  updatePwm = 0;  //Used to update base value of PWM outputs in auto_controller

  // Open file to capture command RX, TX, and pertinent guidance data
  block_txt = opendatafile("block");
  fprintf(block_txt,"timestamp,thrust_m,roll_m,pitch_m,yaw_m,auto_m,thurst_a,roll_a,pitch_a,yaw_a,auto_a,N/A,N/A,N/A,N/A,N/A,N/A\n");

  FILE *gains;
  gains = fopen("gains.txt", "r");
  fscanf(gains, "%f %f %f %f %f %f %f %f %f", &KP_thrust, &KI_thrust, &KD_thrust, &KP_y, &KI_y, &KD_y, &KP_x, &KI_x, &KD_x);
  //printf("Gains: %f, %f, %f, %f, %f, %f, %f, %f\n", KP_thrust, KI_thrust, KD_thrust, KP_roll, KI_roll, KD_roll, KP_pitch, KI_pitch, KD_pitch);
  fclose(gains);

  //Controller Data Filestream
  ctrlData = opendatafile("ctrlData");
  fprintf(ctrlData, "Time, Desired roll (rad), Desired pitch (rad)\n");
  // Initialize the data mutexes
  pthread_mutex_init(&imu_mutex, NULL);
  pthread_mutex_init(&mcap_mutex, NULL);
  pthread_mutex_init(&state_mutex, NULL);
  // pthread_mutex_init(&dynamixel_mutex, NULL);

  // Start the threads
  pthread_t lcm_thread;
  pthread_t processing_thread;
  pthread_t imu_thread;
  pthread_t motion_capture_thread;
  /*pthread_t dynamixel_comm_thread;
  pthread_t dynamixel_set_thread;*/

  pthread_create(&lcm_thread, NULL, lcm_thread_loop, NULL);
  pthread_create(&processing_thread, NULL, processing_loop, NULL);
  pthread_create(&imu_thread, NULL, run_imu, NULL);
  pthread_create(&motion_capture_thread, NULL, run_motion_capture, NULL);
  //pthread_create(&dynamixel_comm_thread, NULL, run_dynamixel_comm, NULL);
  //pthread_create(&dynamixel_set_thread, NULL, set_dynamixel, NULL);

  // Join threads upon completetion
  pthread_join(lcm_thread, NULL);
  pthread_join(processing_thread, NULL);
  pthread_join(imu_thread, NULL);
  pthread_join(motion_capture_thread, NULL);
  //pthread_join(dynamixel_comm_thread, NULL);
  //pthread_join(dynamixel_set_thread, NULL);

  fclose(block_txt);
  fclose(ctrlData);
  return 0;
}

/*
  LCM processing (top-level loop) -- Only use to talk with BLOCKS
*/
void *lcm_thread_loop(void *data){
  lcm_t* lcm = lcm_create(NULL);
  //Set data frequency of blocks to 100Hz
  cfg_data_frequency_t cfg_data_frequency;
  cfg_data_frequency.hz = (uint8_t) 100;
  cfg_data_frequency_t_publish(lcm, "CFG_DATA_FREQUENCY_1_TX", &cfg_data_frequency);
  cfg_data_frequency_t_publish(lcm, "CFG_DATA_FREQUENCY_1_RX", &cfg_data_frequency);
  //Subscribe to blocks channels lcm channel
  channels_t_subscribe(lcm, "CHANNELS_1_RX", channels_handler, lcm);
  while(1)
    lcm_handle(lcm);

  lcm_destroy(lcm);
  return 0;
}

