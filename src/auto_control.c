//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"

// Define outer loop controller 
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up   1550// Upper saturation PWM limit.
#define thrust_PWM_base 1500 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1450 // Lower saturation PWM limit. 
  
// ROLL
/*#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1500  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1380 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1500 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit. 
*/
// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1500 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right). 

#define ROLLPITCHBOUND 120

//#define y_max 12.75
//#define x_max 7.75
#define y_max 5.0
#define x_max 5.0
#define max_euler 0.0873  //5 degrees
#define alt_d 1.3 // in FXB, floor is actually about 0.2m


//Caps for derivative terms
#define MAXDER_ALT  100.0
#define MAXDER_ROLL  70.0
#define MAXDER_PITCH 50.0
// Outer loop controller to generate PWM signals for the Naza-M autopilot

//pose[10] // New Time
//pose[11] // Old Time
void auto_control(float *pose, float *set_points, int16_t* channels_ptr, int16_t* man_channels_ptr){ 

  //                          0  1   2    3     4      5     6     7       8        9         10        11 
  // pose (size 12):  actual {x, y , alt, roll, pitch, xold, yold, altold, rollold, pitchold, time_new, time_old}

  //                       0  1
  // set_points (size 2): {x, y}
  //reference state (you need to set this!) 

   //Baseline values
  static int16_t baseT = man_channels_ptr[0];
  static int16_t baseRoll = man_channels_ptr[1];
  static int16_t basePitch = man_channels_ptr[2];
  static int16_t roll_PWM_left = baseRoll + ROLLPITCHBOUND;
  static int16_t roll_PWM_right = baseRoll - ROLLPITCHBOUND;
  static int16_t pitch_PWM_forward = basePitch + ROLLPITCHBOUND;
  static int16_t pitch_PWM_backward = basePitch - ROLLPITCHBOUND;


  static float intAlt = 0;
  static float intx = 0;
  static float inty = 0;

  if( updatePwm == 1 ){
    printf("Gains: %f %f %f %f %f %f %f %f %f %f %f %f %f \n", KP_thrust, KI_thrust, KD_thrust, KP_y, KI_y, KD_y, KP_x, KI_x, KD_x, KP_roll, KD_roll, KP_pitch, KD_pitch);

    printf("Trim: %d, %d, %d\n", baseT, baseRoll, basePitch);

    updatePwm = 0;
    
    intAlt = 0;
    intx = 0;
    inty = 0;
  } 
 
  // print gains
  
  // Elapsed time since last loop
  //float elapsed = ((pose[10] - pose[11]) * 1000000.0); // constant converts us to s
  static double time_old = 0.0;
  static double time_new = 0.0;

  time_new = ((double)utime_now())/1000000;

  float elapsed = 10000000000000.0;
  if( time_old != 0.0 ){
    elapsed = (float)(time_new - time_old);  
    if(elapsed == 0.0)
      elapsed = 10000000000000.0;
  }
  
  /********************************************************/
  /******************** Thrust Control ********************/
  /********************************************************/

  // Since thrust is positive upward, decided to negate pose[2],
  // pose[2] and posep[7] such that 
  // positive z is "upward" for the controller.

  float errAlt = alt_d - (-pose[2]);
  float alt_der = ( (-pose[2])-(-pose[7]) )/elapsed;
  channels_ptr[0] = baseT + (int16_t)(KP_thrust*errAlt + KI_thrust*intAlt + KD_thrust* -alt_der);

  intAlt = intAlt + errAlt*elapsed;

  /********************************************************/
  /********** Desired Roll and Angle Calculation **********/
  /********************************************************/

  //Desired roll
  float erry = set_points[1] - pose[1];                //(+) if desired > current
  float y_der  = (pose[1] - pose[6])/elapsed;          //
  float y_derder = KD_y* -y_der;
  if( y_derder > 20.0 ) y_derder = 20.0;
  else if( y_derder < -20.0 ) y_derder = -20.0;
  float roll_d = KP_y*erry + KI_y*inty + y_derder;

  inty += erry*elapsed;

  //Desired pitch
  float errx = set_points[0] -  pose[0]; // Flipped because 
  float x_der  = (pose[0] - pose[5])/elapsed;
  float x_derder = KD_x* -x_der;
  if( x_derder > 20.0 ) x_derder = 20.0;
  else if( x_derder < -20.0 ) x_derder = -20.0;
  float pitch_d = KP_x*errx + KI_x*intx + x_derder;

  intx = intx + errx*elapsed;


  /********************************************************/
  /**************** Roll and Angle Control ****************/
  /********************************************************/


  //Roll Output
  channels_ptr[1] = baseRoll + -( (int16_t) roll_d ); // negative because stick commands are opposite

  // Pitch Output
  channels_ptr[2] = basePitch + (int16_t) pitch_d;

  //Bound derivative terms to "filter" out large jolts
  /*if(derAlt > MAXDER_ALT) derAlt = MAXDER_ALT;
  else if(derAlt < -MAXDER_ALT) derAlt = -MAXDER_ALT;

  if(derRoll > MAXDER_ROLL) derRoll = MAXDER_ROLL;
  else if(derRoll < -MAXDER_ROLL) derRoll = -MAXDER_ROLL;

  if(derPitch > MAXDER_PITCH) derPitch = MAXDER_PITCH;
  else if(derPitch < -MAXDER_PITCH) derPitch = -MAXDER_PITCH;*/

  //printf( "Control: %f, %f, %f, %f, %f. %d, %d, %d. %d, %d, %d. \n", errx, erry, errAlt, roll_d, pitch_d, channels_ptr[1], channels_ptr[2], channels_ptr[0], man_channels_ptr[1], man_channels_ptr[2], man_channels_ptr[0] );

  // Final Stick Control Bounds

  if( channels_ptr[0] > thrust_PWM_up )           channels_ptr[0] = thrust_PWM_up;
  else if( channels_ptr[0] < thrust_PWM_down )    channels_ptr[0] = thrust_PWM_down;
  if( channels_ptr[1] > roll_PWM_left )           channels_ptr[1] = roll_PWM_right;
  else if( channels_ptr[1] <  roll_PWM_right )    channels_ptr[1] = roll_PWM_left;
  if( channels_ptr[2] > pitch_PWM_forward )       channels_ptr[2] = pitch_PWM_forward;
  else if( channels_ptr[2] < pitch_PWM_backward ) channels_ptr[2] = pitch_PWM_backward;

  // Data Logging
  fprintf(ctrlData, "%"PRId64", %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf\n",
          utime_now(), roll_d, pitch_d, alt_d, errx, erry, errAlt, x_der , y_der, alt_der);

  time_old = ((double)utime_now())/1000000;

  return;
}
