// Handler function to either pass through RX commands to Naza or else
// copy computer (autonomous control) commands through to Naza.
#define EXTERN extern

#include "../include/quadcopter_main.h"

////////////////////////////////////////////////////////////////////////

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata)
{
  

  // create a copy of the received message
  channels_t new_msg;
  new_msg.utime = msg->utime;
  new_msg.num_channels = msg->num_channels;
  new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
  for(int i = 0; i < msg->num_channels; i++){
    new_msg.channels[i] = msg->channels[i];
  }

  static int16_t trim[] = {new_msg.channels[0], new_msg.channels[1], new_msg.channels[2]};

  // Copy state to local state struct to minimize mutex lock time
  struct state localstate;
  pthread_mutex_lock(&state_mutex);
    if(msg->channels[7] > 1500){
      state->fence_on = 1;
    }
    else{
      state->fence_on = 0;
    }
  memcpy(&localstate, state, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);
  // Decide whether or not to edit the motor message prior to sending it
  // set_points[] array is specific to geofencing.  You need to add code 
  // to compute them for our FlightLab application!!!

  float pose[11], set_points[2];

  //double elapsed = (localstate.pose[10] - localstate.pose[11]) / 1000000.0;
  if(localstate.fence_on == 1){
    for(int i = 0; i < 11; i++){
      pose[i] = (float) localstate.pose[i];
    }
    //pose[10] = (float) elapsed;

    for( int i = 0; i < 2; i++ ){
      set_points[i] = localstate.set_points[i];
    }
    // hold position at edge of fence
    // This needs to change - mutex held way too long
    
    auto_control(pose, set_points, new_msg.channels, trim);
    //printf("AUTONOMOUS ON\n");

  } else{  // Fence off
    // pass user commands through without modifying
    //printf("MANUAL\n");
  }

  // send lcm message to motors
  channels_t_publish((lcm_t *) userdata, "CHANNELS_1_TX", &new_msg);



  // Save received (msg) and modified (new_msg) command data to file.
  // NOTE:  Customize as needed (set_points[] is for geofencing)
  //printf("     Command: %d, %d, %d \n", new_msg.channels[0], new_msg.channels[1], new_msg.channels[2]);
  fprintf(block_txt,"%"PRId64",%d,%d,%d,%d,%d,%d,%d,%d,%f,%f\n",
	  utime_now(),
    msg->channels[0], msg->channels[1], msg->channels[2], msg->channels[7],
	  new_msg.channels[0], new_msg.channels[1], new_msg.channels[2], new_msg.channels[7],
	  set_points[0],set_points[1]);
  fflush(block_txt);
}
